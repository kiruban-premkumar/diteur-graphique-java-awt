import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Vector;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 * Classe ControlMenuItemOuvrir
 * @author Adrien BROYERE
 * @author Kiruban PREMKUMAR
 */

public class ControlMenuItemOuvrir implements ActionListener 
{
	/**
	 * Fenetre des elements graphiques
	 */
	private WindowDisplay frame;
	
	/**
	 * Constructeur
	 * @param f Fenetre des elements graphiques
	 */
	public ControlMenuItemOuvrir(WindowDisplay f) {
		frame = f;
	}

	/**
	 * Lecture des figures depuis un fichier et 
	 * restauration de la fenetre d'affichage des figures
	 * @param e ActionEvent
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void actionPerformed(ActionEvent e) {
		JFileChooser chooser = new JFileChooser();
        int dialReturn = chooser.showOpenDialog(frame);
        if (dialReturn == JFileChooser.CANCEL_OPTION)
        	return;
        if (dialReturn == JFileChooser.APPROVE_OPTION)
        	frame.setFilePath(chooser.getCurrentDirectory() + "/" + chooser.getSelectedFile().getName());

        ObjectInputStream stream = null;
        try {
                stream = new ObjectInputStream(new FileInputStream(
                                frame.filePath));
                try {
                        frame.figs = (Vector<FigureGraphique>) stream.readObject();
                        frame.da.repaint();

                } catch (ClassNotFoundException err) {
                        JOptionPane.showMessageDialog(frame, "Erreur d'ouverture du fichier",
                                      "Erreur",
                                      JOptionPane.WARNING_MESSAGE);
                        frame.filePath = null;
                } catch (NullPointerException err) {
                        JOptionPane.showMessageDialog(frame, "Erreur d'ouverture du fichier",
                                      "Erreur",
                                      JOptionPane.WARNING_MESSAGE);
                        frame.filePath = null;
                }
        } catch (FileNotFoundException err) {
                JOptionPane.showMessageDialog(frame, "Fichier non trouv�", "Erreur", JOptionPane.WARNING_MESSAGE);
                frame.filePath = null;
        } catch (IOException e1) {
        	 JOptionPane.showMessageDialog(frame, "Fichier incorrect", "Erreur", JOptionPane.WARNING_MESSAGE);
        	 frame.filePath = null;
		}

	}

}
