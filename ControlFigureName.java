import java.awt.event.*;

/**
 * Classe ControlFigureName
 * @author Adrien BROYERE
 * @author Kiruban PREMKUMAR
 */

class ControlFigureName implements KeyListener
{
	/**
	 * La barre d'outils
	 */
	protected Toolbar toolbar;
	
	/**
	 * Fenetre des elements graphiques
	 */
	protected WindowDisplay frame;
	
	/**
	 * Constructeur
	 * @param toolbar La barre d'outil
	 * @param frame La fenetre d'affichage des elements graphiques
	 */
	public ControlFigureName (Toolbar toolbar, WindowDisplay frame)
	{
		this.toolbar = toolbar;
		this.frame = frame;
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * Change le nom des figures
	 * @param arg0 le KeyEvent
	 */
	@Override
	public void keyTyped(KeyEvent arg0) {
		toolbar.changeLabel();
		for (int i = 0; i < frame.selectedFigs.size(); i++)
		{
			frame.selectedFigs.get(i).name = frame.figureName;
			frame.da.repaint();
		}
	}


}
