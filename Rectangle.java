import java.awt.Color;
import java.awt.Graphics;
import java.io.Serializable;

/**
 * Classe Rectangle
 * @author Adrien BROYERE
 * @author Kiruban PREMKUMAR
 */

public class Rectangle extends FigureGraphique implements Serializable
{
	/**
	 * The point on the upper left corner of the rectangle
	 */
	protected Point2D point;
	
	/**
	 * Largeur du rectangle
	 */
	protected int width;
	
	/**
	 * Hauteur du rectangle
	 */
	protected int height;
	
	/**
	 * Constructeur
	 * @param name Nom du rectangle
	 * @param outlineColor Couleur de contour
	 * @param fillColor Couleur de remplissage
	 * @param x Coordonnée x du premier point
	 * @param y Coordonnée y du premier point
	 * @param width Largeur du rectangle
	 * @param height Hauteur du rectangle
	 */
	public Rectangle(String name, Color outlineColor, Color fillColor, int x, int y, int width, int height)
	{
		super(name, outlineColor, fillColor);
		
		point = new Point2D(x, y);
		this.width = width;
		this.height = height;
	}
	
	/**
	 * Retourne les informations du rectangle
	 * @return Les details d'un rectangle, sous la forme d'une chaine de carateres
	 */
	public String toString()
	{
		return (new String("Rectangle : " + this.name + "\tPoint gauche haut : " + this.point.toString() +
			"\tlargeur : " + this.width + ", hauteur : " + this.height + "\ncouleur contour : " + this.outlineColor +
			", couleur remplissage : " + this.fillColor));

	}
	
	/**
	 * Retourne la surface du rectangle
	 * @return La surface d'un rectangle, sous la forme d'un double
	 */
	@Override
	public double getSurface() {
		// TODO Auto-generated method stub
		return (this.height * this.width);
	}
	
	/**
	 * Retourne le centre du rectangle
	 * @return Le centre du rectangle, sous la forme d'un objet Point2D
	 */
	@Override
	public Point2D getCenter()
	{
		// TODO Auto-generated method stub
		return (new Point2D (this.point.getX() + (this.width) / 2, this.point.getY() + (this.height) / 2));
	}
	
	/**
	 * Getter
	 * @return Hauteur du rectangle
	 */
	public int getHeight()
	{
		return this.height;
	}
	
	/**
	 * Getter
	 * @return Largeur du rectangle
	 */
	public int getWidth()
	{
		return this.width;
	}
	
	/**
	 * Deplace un rectangle
	 * @param x Position sur l'axe X
	 * @param y Position sur l'axe Y
	 */
	@Override
	public void move(int x, int y)
	{
		// TODO Auto-generated method stub
		this.point.move(x, y);
	}
	
	/**
	 * Verifie si un point est contenu dans le rectangle
	 * @param p un point de type Point2D
	 * @return True si le point p est contenu dans le rectangle
	 * 		   False sinon
	 */
	@Override
	public boolean contain(Point2D p)
	{
		// TODO Auto-generated method stub
		if (p.getX() >= point.getX() &&
			p.getX() <= point.getX() + width &&
			p.getY() >= point.getY() &&
			p.getY() <= point.getY() + height)
			return true;
		else
			return false;
	}
	
	/**
	 * Dessine le rectangle
	 * @param g Composant Graphics servant au dessin
	 */
	@Override
	public void drawYourself(Graphics g) {
		// TODO Auto-generated method stub
		
		//	Fill color
		g.setColor(this.fillColor);              
		g.fillRect(this.point.x, this.point.y, this.width, this.height); 
		
		//	Outline color
		g.setColor(this.outlineColor);
		g.drawRect(this.point.x, this.point.y, this.width, this.height);
		
		//	Draw the name of the rectangle
		g.drawString(this.name, this.getCenter().x, this.getCenter().y);   
	}
}
