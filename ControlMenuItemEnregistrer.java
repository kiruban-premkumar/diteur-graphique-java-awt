import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 * Classe ControlMenuItemEnregistrer
 * @author Adrien BROYERE
 * @author Kiruban PREMKUMAR
 */

public class ControlMenuItemEnregistrer implements ActionListener 
{
	/**
	 * Fenetre des elements graphiques
	 */
	private WindowDisplay frame;
	
	/**
	 * Boolean en cas d'annulation de l'enregistrement des figures dans un fichier
	 */
	private boolean cancel;
	
	/**
	 * Constructeur
	 * @param f Fenetre des elements graphiques
	 */
	public ControlMenuItemEnregistrer(WindowDisplay f) {
		this.frame = f;
		cancel = false;
	}
	
	/**
	 * Enregistrement des figures dans un fichier
	 * @param e ActionEvent
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		
		int r;
		ObjectOutputStream stream = null;
		
		if (frame.isFileNotSaved()) {

			JFileChooser chooser = new JFileChooser();
			int dialReturn = chooser.showSaveDialog(chooser);
	
			if (dialReturn == JFileChooser.APPROVE_OPTION) {
	
				File f = chooser.getSelectedFile();
		        if (f.exists()) {
		        	r = JOptionPane.showConfirmDialog(frame,"Voulez vous �crassez le fichier " + chooser.getSelectedFile().getName() + "?", "Oui", 2);
		        	if (r == JOptionPane.OK_OPTION)
		        		frame.setFilePath(chooser.getCurrentDirectory() + System.getProperty("file.separator") + chooser.getSelectedFile().getName());
		        	else
                        cancel = true;
		        } 
		        else
		        	frame.setFilePath(chooser.getCurrentDirectory() + System.getProperty("file.separator") + chooser.getSelectedFile().getName());
			}
			
			if (dialReturn == JFileChooser.CANCEL_OPTION)
				cancel = true;
		}
		
		if (cancel != true) {
		
	        try {
	        	stream = new ObjectOutputStream(new FileOutputStream(frame.filePath));
	        } catch (FileNotFoundException e1) {
	        	e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			try {
		        stream.writeObject(this.frame.getFigs());
		        System.out.println(frame.filePath + " enregistr�!");
		        frame.setFilePath(frame.filePath);
			} catch (IOException e1) {
                e1.printStackTrace();
			}
		
		}

	}

}
