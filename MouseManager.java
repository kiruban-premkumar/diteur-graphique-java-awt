import java.awt.event.*;

/**
 * Classe MouseManager
 * @author Adrien BROYERE
 * @author Kiruban PREMKUMAR
 */

class MouseManager extends MouseAdapter
{
	/**
	 * Fenetre des elements graphiques
	 */
	protected WindowDisplay frame;
	
	/**
	 * Constructeur
	 * @param frame Fenetre des elements graphiques
	 */
	public MouseManager(WindowDisplay frame)
	{
		this.frame = frame;
	}
	
	/**
	 * Gestion de l'Úvenement mousePressed
	 * @param e MouseEvent
	 */
	public void mousePressed(MouseEvent e)
	{
		int button = e.getButton();
		
		if (button == MouseEvent.BUTTON3)
		{
			frame.boutonDroitSourisEnfonce(e.getX(), e.getY());
		}
		else
		{
			frame.boutonSourisEnfonce(e.getX(),e.getY());
		}
	}
	
	/**
	 * Gestion de l'Úvenement mouseReleased
 	 * @param e MouseEvent
	 */
	public void mouseReleased(MouseEvent e)
	{
		int button = e.getButton();
		
		if (button == MouseEvent.BUTTON3)
		{
			frame.boutonDroitSourisRelache(e.getX(), e.getY());
		}
		else
		{
			frame.boutonSourisRelache(e.getX(), e.getY());
		}
	}
}