import java.awt.*;
import java.awt.event.*;

/**
 * Classe ControlItemCouleur
 * @author Adrien BROYERE
 * @author Kiruban PREMKUMAR
 */

class ControlItemCouleur implements ItemListener 
{
	/**
	 * Fenetre des elements graphiques
	 */
	protected WindowDisplay frame;
	
	/**
	 * Liste des couleurs
	 */
	Color couleurs[];
	
	/**
	 * Le choix : si c'est contour ou remplissage
	 */
	String choix;
	
	/**
	 * Constructeur
	 * @param couleurs Liste des couleurs
	 * @param frame La fenetre d'affichage des elements graphiques
	 * @param choix Si c'est contour ou remplissage
	 */
	public ControlItemCouleur (Color couleurs[], WindowDisplay frame, String choix)
	{
		this.couleurs = couleurs;
		this.frame = frame;
		this.choix = choix;
	}
	
	/**
	 * Listener pour la gestion de la selection des elements 
	 * des listes deroulantes pour la couleur de contour 
	 * et de remplissage
	 * @param e ItemEvent
	 */
	public void itemStateChanged(ItemEvent e)
	{
		//$$7
		Choice liste = (Choice)e.getSource();
		
		if (this.choix == "contour")
			frame.changeCouleurContour(couleurs[liste.getSelectedIndex()]);
		else
			frame.changeCouleurRemplissage(couleurs[liste.getSelectedIndex()]);
		
	}
}
