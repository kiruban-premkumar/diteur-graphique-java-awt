import java.awt.*;
import java.awt.event.*;

/**
 * Classe ControlItemFigure
 * @author Adrien BROYERE
 * @author Kiruban PREMKUMAR
 */

class ControlItemFigure implements ItemListener 
{
	/**
	 * Fenetre des elements graphiques
	 */
	protected WindowDisplay frame;
	
	/**
	 * Liste des figures
	 */
	String figures[];
	
	/**
	 * Constructeur
	 * @param figures Liste des figures
	 * @param frame La fenetre d'affichage des elements graphiques
	 */
	public ControlItemFigure (String figures[], WindowDisplay frame)
	{
		this.figures = figures;
		this.frame = frame;
	}
	
	/**
	 * Listener pour la selection d'une figure
	 * @param e ItemEvent
	 */
	public void itemStateChanged(ItemEvent e)
	{
		//$$7
		Choice liste = (Choice)e.getSource();
		frame.changeFigure(figures[liste.getSelectedIndex()]);		
	}
}