import java.awt.Graphics;

/**
 * Interface Figure
 * @author Adrien BROYERE
 * @author Kiruban PREMKUMAR
 */

public interface Figure
{
	/**
	 * Retourne la surface d'une figure
	 * @return La surface d'une figure, sous la forme d'un double
	 */
	public abstract double getSurface();
	
	/**
	 * Retourne le centre ou barycentre d'une figure
	 * @return Le centre ou barycentre d'une figure, sous la forme d'un objet Point2D
	 */
	public abstract Point2D getCenter();
	
	/**
	 * Deplace une figure
	 * @param x Position sur l'axe X
	 * @param y Position sur l'axe Y
	 */
	public abstract void move(int x, int y);
	
	/**
	 * Verifie si un point est contenu dans une figure
	 * @param p un point de type Point2D
	 * @return True si le point p est contenu dans la figure
	 * 		   False sinon
	 */
	public abstract boolean contain(Point2D p);
	
	/**
	 * Retourne les informations de la figure
	 * @return Les details d'une figure, sous la forme d'une chaine de carateres
	 */
	public abstract String toString();
	
	/**
	 * Dessine la figure
	 * @param g Composant Graphics servant au dessin
	 */
	public abstract void drawYourself(Graphics g);
}	
