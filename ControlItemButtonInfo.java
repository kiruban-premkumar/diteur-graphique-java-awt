import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

/**
 * Classe ControlMenuItemEnregistrer
 * @author Adrien BROYERE
 * @author Kiruban PREMKUMAR
 */

public class ControlItemButtonInfo implements ActionListener {
	
	/**
	 * Fenetre des elements graphiques
	 */
	private WindowDisplay frame;
	
	/**
	 * Constructeur
	 * @param f Fenetre des elements graphiques
	 */
	public ControlItemButtonInfo(WindowDisplay f)
	{
		this.frame = f;
	}
	
	/**
	 * Affichage des centres / barycentres
	 * @param e ActionEvent
	 */
	@Override
	public void actionPerformed(ActionEvent arg0) {
		String output = "";
		// Run through the selected figs
		for (int i = 0; i < frame.selectedFigs.size(); i++)
		{
			output += frame.selectedFigs.get(i).name;
			if(frame.selectedFigs.get(i) instanceof Rectangle || frame.selectedFigs.get(i) instanceof Circle)
				output +=  " de centre";
			else
				output += " de barycentre";
			
			output += "("+frame.selectedFigs.get(i).getCenter().x+","+frame.selectedFigs.get(i).getCenter().y+")\n";
		}
		
		if(output.isEmpty())
			JOptionPane.showMessageDialog(frame, "Pas de figure selectionnée");
		else
			JOptionPane.showMessageDialog(frame, output);
	}

}
