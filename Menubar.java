import java.awt.*;

/**
 * Classe Menubar
 * @author Adrien BROYERE
 * @author Kiruban PREMKUMAR
 */

public class Menubar extends MenuBar {
		
	/**
	 * MenuItem pour l'enregistrement
	 */
	public MenuItem enregistrer;
	
	/**
	 * MenuItem pour quitter l'application
	 */
	public MenuItem quitter;
	
	/**
	 * MenuItem pour ouvrir un fichier enregist�
	 */
	public MenuItem ouvrir;
	
	/**
	 * Constructeur
	 * @param f Fenetre d'affichage des figures
	 */
	public Menubar(WindowDisplay f) {
		Menu menuFichier = new Menu("Fichier");
		
		ouvrir = new MenuItem("Ouvrir");
		ouvrir.addActionListener(new ControlMenuItemOuvrir(f));
		
		enregistrer = new MenuItem("Enregistrer");
		enregistrer.addActionListener(new ControlMenuItemEnregistrer(f));
		
		quitter = new MenuItem("Quitter");
		quitter.addActionListener(new ControlMenuItemQuitter(f));
		
		menuFichier.add(ouvrir);
		menuFichier.add(enregistrer);
		menuFichier.add(quitter);
		add(menuFichier);
	}
}