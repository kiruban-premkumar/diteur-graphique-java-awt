import java.awt.event.*;

/**
 * Classe ControlSupprFigure
 * @author Adrien BROYERE
 * @author Kiruban PREMKUMAR
 */

public class ControlSupprFigure implements KeyListener
{
	/**
	 * Fenetre des elements graphiques
	 */
	protected WindowDisplay frame;
	
	/**
	 * Constructeur
	 * @param frame Fenetre des elements graphiques
	 */
	public ControlSupprFigure(WindowDisplay frame)
	{
		this.frame = frame;
	}
	
	/**
	 * Listener pour supprimer les figures selectionnees
	 * @param e ActionEvent
	 */
	@Override
	public void keyPressed(KeyEvent e)
	{
		// TODO Auto-generated method stub
		if (e.getKeyCode() == KeyEvent.VK_DELETE)
		{
			frame.deleteFigs();
		}
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
