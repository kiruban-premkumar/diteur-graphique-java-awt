import java.awt.Color;
import java.awt.Graphics;
import java.io.Serializable;

/**
 * Classe abstraite FigureGraphique
 * @author Adrien BROYERE
 * @author Kiruban PREMKUMAR
 */

public abstract class FigureGraphique implements Figure, Serializable
{
	/**
	 * Couleur de contour
	 **/
	Color outlineColor;
	
	/**
	 * Couleur de remplissage
	 **/
	Color fillColor;
	
	/**
	 * Nom de la figure
	 **/
	String name;
	
	/**
	 * Constructeur
	 * @param name Nom de la figure
	 * @param outlineColor Couleur de contour
	 * @param fillColor Couleur de remplissage
	 */
	public FigureGraphique(String name, Color outlineColor, Color fillColor)
	{
		this.outlineColor = outlineColor;
		this.fillColor = fillColor;
		this.name = name;
	}
	
	/**
	 * Getter
	 * @return La couleur de contour
	 */
	public Color getOutlineColor()
	{
		return this.outlineColor;
	}
	
	/**
	 * Getter
	 * @return La couleur de remplissage
	 */
	public Color getFillColor()
	{
		return this.fillColor;
	}
	
	/**
	 * Distance entre deux figures
	 * @param f1 Figure 1
	 * @param f2 Figure 2
	 * @return La distance entre les centres/barycentres de f1 et f2
	 */
	public static double distance(Figure f1, Figure f2)
	{
		return Point2D.distance(f1.getCenter(), f2.getCenter());
	}
	
	/**
	 * Dessine la figure
	 * @param g Composant Graphics servant au dessin
	 */
	public abstract void drawYourself(Graphics g);

}
