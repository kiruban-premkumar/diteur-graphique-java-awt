import java.awt.event.*;

/**
 * Classe WindowController
 * @author Adrien BROYERE
 * @author Kiruban PREMKUMAR
 */

class WindowController extends WindowAdapter  
{
	/**
	 * Fenetre des elements graphiques
	 */
	WindowDisplay fenet;
	
	/**
	 * Constructeur
	 * @param fenet Fenetre des elements graphiques
	 */
	public WindowController(WindowDisplay fenet)
	{
		this.fenet = fenet;
	}
	
	/**
	 * Fonction permettant la fermeture de l'application
	 */
	public void windowClosing(WindowEvent e)
	{
		//$$1 - fait
		System.exit(0);
	}
}