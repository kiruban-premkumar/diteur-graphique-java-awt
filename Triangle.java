import java.awt.Color;
import java.awt.Graphics;
import java.io.Serializable;

/**
 * Classe Triangle
 * @author Adrien BROYERE
 * @author Kiruban PREMKUMAR
 */

public class Triangle extends FigureGraphique implements Serializable
{
	/**
	 * Les trois sommets du triangles
	 */
	protected Point2D p1, p2, p3;
	
	/**
	 * Constructeur 1
	 * @param name Nom du triangle
	 * @param outlineColor Couleur de contour
	 * @param fillColor Couleur de remplissage
	 * @param x1 Coordonnée x sommet1
	 * @param y1 Coordonnée y sommet1
	 * @param x2 Coordonnée x sommet2
	 * @param y2 Coordonnée y sommet2
	 * @param x3 Coordonnée x sommet3
	 * @param y3 Coordonnée y sommet3
	 */
	public Triangle(String name, Color outlineColor, Color fillColor, int x1, int y1, int x2, int y2, int x3, int y3)
	{
		super(name, outlineColor, fillColor);
		
		this.p1 = new Point2D(x1, y1);
		this.p2 = new Point2D(x2, y2);
		this.p3 = new Point2D(x3, y3);
	}
	
	/**
	 * Constructeur 2
	 * @param name Nom du triangle
	 * @param outlineColor Couleur de contour
	 * @param fillColor Couleur de remplissage
	 * @param p1 Coordonnées Point2D du sommet1
	 * @param p2 Coordonnées Point2D du sommet2
	 * @param p3 Coordonnées Point2D du sommet3
	 */
	public Triangle(String name, Color outlineColor, Color fillColor, Point2D p1, Point2D p2, Point2D p3)
	{
		super(name, outlineColor, fillColor);
		
		this.p1 = new Point2D(p1);
		this.p2 = new Point2D(p2);
		this.p3 = new Point2D(p3);
	}
	
	/**
	 * Constructeur 3
	 * @param outlineColor Couleur de contour
	 * @param fillColor Couleur de remplissage
	 * @param p1 Coordonnées Point2D du sommet1
	 * @param p2 Coordonnées Point2D du sommet2
	 * @param p3 Coordonnées Point2D du sommet3
	 */
	public Triangle(Color outlineColor, Color fillColor, Point2D p1, Point2D p2, Point2D p3)
	{
		super("", outlineColor, fillColor);
		
		this.p1 = new Point2D(p1);
		this.p2 = new Point2D(p2);
		this.p3 = new Point2D(p3);
	}
	
	/**
	 * Retourne les informations du triangle
	 * @return Les details d'un triangle, sous la forme d'une chaine de carateres
	 */
	public String toString()
	{
		return (new String("Triangle : " + name + "\nPoints :\n" + this.p1.toString() + "\n" + this.p2.toString() +
				"\n" + this.p3.toString() + "\nCouleur contour : " + outlineColor + "\nCouleur remplissage : " + fillColor));
	}

	/**
	 * Retourne la surface du triangle
	 * @return La surface d'un triangle, sous la forme d'un double
	 */
	@Override
	public double getSurface()
	{
		// TODO Auto-generated method stub
		
		//	Heron's formula
		
		//	Get the value of the three sides of the triangle
		double side1, side2, side3;
		side1 = Math.abs(this.p1.distance(this.p2));
		side2 = Math.abs(this.p2.distance(this.p3));
		side3 = Math.abs(this.p3.distance(this.p1));
		
		double s = 0.5 * (side1 + side2 + side3);
		
		double surface = Math.sqrt(s*(s - side1) * (s - side2) * (s - side3));
		
		return surface;
	}

	/**
	 * Retourne le barycentre du triangle
	 * @return Le barycentre du triangle, sous la forme d'un objet Point2D
	 */
	@Override
	public Point2D getCenter()
	{
		// TODO Auto-generated method stub
		Point2D centroid = new Point2D();
	
		centroid.x = (this.p1.x + this.p2.x + this.p3.x) / 3;
		centroid.y = (this.p1.y + this.p2.y + this.p3.y) / 3;
	
		return centroid;
	}
	
	/**
	 * Deplace un triangle
	 * @param x Position sur l'axe X
	 * @param y Position sur l'axe Y
	 */
	@Override
	public void move(int x, int y)
	{
		p1.move(x, y);
		p2.move(x, y);
		p3.move(x, y);
	}
	
	/**
	 * Verifie si un point est contenu dans le triangle
	 * @param p un point de type Point2D
	 * @return True si le point p est contenu dans le triangle
	 * 		   False sinon
	 */
	@Override
	public boolean contain(Point2D p)
	{
		// TODO Auto-generated method stub
		
		double z1 = this.p1.x * (this.p2.y - p.getY()) + this.p2.x * (p.getY() - this.p1.y) + p.getX() * (this.p1.y - this.p2.y);
		double z2 = this.p2.x * (this.p3.y - p.getY()) + this.p3.x * (p.getY() - this.p2.y) + p.getX() * (this.p2.y - this.p3.y);
		double z3 = this.p3.x * (this.p1.y - p.getY()) + this.p1.x * (p.getY() - this.p3.y) + p.getX() * (this.p3.y - this.p1.y);

		if ((z1 > 0 && z2 > 0 && z3 > 0) || (z1 < 0 && z2 < 0 && z3 < 0))
			return true;
		else
			return false;
	}
	
	/**
	 * Dessine le triangle
	 * @param g Composant Graphics servant au dessin
	 */
	@Override
	public void drawYourself(Graphics g)
	{
		// TODO Auto-generated method stub
		
		//	Fill Color
		g.setColor(this.fillColor);
		int x[] = {this.p1.x, this.p2.x, this.p3.x};
		int y[] = {this.p1.y, this.p2.y, this.p3.y};
		int nbPoints = 3;
		g.fillPolygon(x, y, nbPoints);
		
		//	Outline Color
		g.setColor(this.outlineColor);
		g.drawLine(this.p1.x, this.p1.y, this.p2.x, this.p2.y);
		g.drawLine(this.p2.x, this.p2.y, this.p3.x, this.p3.y);
		g.drawLine(this.p3.x, this.p3.y, this.p1.x, this.p1.y);
		
		//	Draw the name of the rectangle
		g.drawString(this.name, this.getCenter().x, this.getCenter().y);   
	}
}
