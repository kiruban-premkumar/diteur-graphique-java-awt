import java.awt.Color;
import java.awt.Graphics;
import java.io.Serializable;

/**
 * Classe Cercle
 * @author Adrien BROYERE
 * @author Kiruban PREMKUMAR
 */
public class Circle extends FigureGraphique implements Serializable
{
	/**
	 * Le centre du cercle
	 */
	protected Point2D center;
	
	/**
	 * Le rayon du cercle
	 */
	protected int radius;

	/**
	 * Constructeur du cercle
	 * @param name Nom du cercle
	 * @param outlineColor Couleur de contour
	 * @param fillColor Couleur de remplissage
	 * @param x Position sur l'axe X
	 * @param y Position sur l'axe Y
	 * @param radius Rayon du cercle
	 */
	public Circle(String name, Color outlineColor, Color fillColor, int x, int y, int radius)
	{
		super(name, outlineColor, fillColor);
		this.center = new Point2D(x, y);
		this.radius = radius;
	}
	
	/**
	 * Retourne les informations du cercle
	 * @return Les details d'un cercle, sous la forme d'une chaine de carateres
	 */
	public String toString()
	{
		return (new String("Cercle : " + this.name + "\tcentre : "+ this.center.toString() +
				"\trayon : "+ this.radius + "\ncouleur contour : " + this.outlineColor +
				", couleur remplissage : " + this.fillColor));
	}
	
	/**
	 * Retourne la surface du cercle
	 * @return La surface d'un cercle, sous la forme d'un double
	 */
	@Override
	public double getSurface()
	{
		// TODO Auto-generated method stub
		return Math.PI * this.radius * this.radius;
	}

	/**
	 * Retourne le centre du cercle
	 * @return Le centre du cercle, sous la forme d'un objet Point2D
	 */
	@Override
	public Point2D getCenter()
	{
		// TODO Auto-generated method stub
		return this.center;
	}

	/**
	 * Deplace un cercle
	 * @param x Position sur l'axe X
	 * @param y Position sur l'axe Y
	 */
	@Override
	public void move(int x, int y)
	{
		// TODO Auto-generated method stub
		this.center.move(x, y);
	}

	/**
	 * Verifie si un point est contenu dans le cercle
	 * @param p un point de type Point2D
	 * @return True si le point p est contenu dans le cercle
	 * 		   False sinon
	 */
	@Override
	public boolean contain(Point2D p)
	{
		// TODO Auto-generated method stub
		if (this.center.distance(p) < this.radius)
			return true;
		else
			return false;
	}

	/**
	 * Dessine le cercle
	 * @param g Composant Graphics servant au dessin
	 */
	@Override
	public void drawYourself(Graphics g)
	{
		// TODO Auto-generated method stub
		
		//	Fill Color
		g.setColor(fillColor);
		g.fillOval((int)(center.x - radius), (int)(center.y - radius), radius * 2, radius * 2);
		
		//	Outline Color
		g.setColor(this.outlineColor);
		g.drawOval((int)(center.x - radius), (int)(center.y - radius), radius * 2, radius * 2);
		
		//	Draw the name of the circle
		g.drawString(name, center.x, center.y);
	}
}
