import java.awt.*;

/**
 * Classe Toolbar
 * @author Adrien BROYERE
 * @author Kiruban PREMKUMAR
 */

class Toolbar  extends Panel
{
	/**
	 * TextField pour taper le nom des figures
	 */
	private TextField nomFigure;
	
	/**
	 * Fenetre des elements graphiques
	 */
	private WindowDisplay frame;
	
	//$$6 - fait
	/**
	 * Constructeur
	 * @param f Fenetre des elements graphiques
	 */
	public Toolbar(WindowDisplay f)
	{
		frame = f;
		Choice listeCouleurContour, listeCouleurRemplissage, listeFigure;
		
		String libelleCouleur[] = {"blanc", "rouge", "jaune", "vert", "bleu", "noir"};
		Color couleurs[] = {Color.white, Color.red, Color.yellow, Color.green, Color.blue, Color.black};
		String figures[] = {"Cercle","Rectangle","Triangle","Polygone"};
		
		listeCouleurContour = new Choice();
		listeCouleurRemplissage = new Choice();
		listeFigure = new Choice();
		
		for (int i = 0; i < libelleCouleur.length; i++)
		{
			listeCouleurContour.addItem(libelleCouleur[i]);
			listeCouleurRemplissage.addItem(libelleCouleur[i]);
		}
		
		for(int i = 0; i < figures.length; i++)
			listeFigure.addItem(figures[i]);
		
		listeCouleurContour.select(0);
		listeCouleurRemplissage.select(0);
		listeFigure.select(0);
		
		add(new Label("Contour"));
		add(listeCouleurContour);
		
		add(new Label("Remplissage"));
		add(listeCouleurRemplissage);
		
		add(new Label("Figure"));
		add(listeFigure);
		
		
		add(new Label("Nom figure:"));
		nomFigure=new TextField("",15);
		nomFigure.setForeground(Color.red);
		add(nomFigure);
		
		// Boutton pour les centres/barycentres
        Button info = new Button("Centre/Barycentre");
        add(info);
        info.addActionListener(new ControlItemButtonInfo(frame));
		
		listeCouleurContour.addItemListener(new ControlItemCouleur(couleurs, frame, "contour"));
		listeCouleurRemplissage.addItemListener(new ControlItemCouleur(couleurs, frame, "remplissage"));
		listeFigure.addItemListener(new ControlItemFigure(figures, frame));
		nomFigure.addKeyListener(new ControlFigureName(this,frame));
	}
	
	/**
	 * Methode appel� par un KeyListener pour changer le nom des figures
	 */
	public void changeLabel() 
	{
		nomFigure.setForeground(Color.black);
		frame.changeFigureName(nomFigure.getText());
	}
	
}
