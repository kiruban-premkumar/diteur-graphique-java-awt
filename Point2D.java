import java.io.Serializable;

/**
 * Classe Point2D
 * @author Adrien BROYERE
 * @author Kiruban PREMKUMAR
 */

class Point2D implements Serializable
{
	/**
	 * Position x du Point2D
	 */
	protected int x;
	
	/**
	 * Position y du Point2D
	 */
	protected int y;
	
	/**
	 * Nombre de points
	 */
	protected static int nbPoints = 0;
	
	/**
	 * Epsilon
	 */
	protected static final int epsilon = 5;

	/**
	 * Default Constructor
	 */
	public Point2D()
	{
		this.x = 0;
		this.y = 0;
		nbPoints++;
	}

	/**
	 * Constructor
	 * @param x position X
	 * @param y position Y
	 */
	public Point2D(int x, int y)
	{
		this.x = x;
		this.y = y;
		nbPoints++;
	}

	/**
	 * Copy constructor
	 * @param p Point2D � copier
	 */
	public Point2D(Point2D p)
	{
		this.x = p.x;
		this.y = p.y;
		nbPoints ++;
	}

	/**
	 * Clean before destruction of the object
	 */
	protected void finalize()
	{
		nbPoints--;
	}
	
	/**
	 * Getter
	 * @return x
	 */
	public int getX()
	{
		return this.x;
	}
	
	/**
	 * Getter
	 * @return y
	 */
	public int getY()
	{
		return this.y;
	}
	
	/**
	 * Setter X
	 */
	public void setX(int value)
	{
		this.x = value;
	}
	
	/**
	 * Setter Y
	 */
	public void setY(int value)
	{
		this.y = value;
	}
	
	/**
	 * Getter
	 * @return int le nombre de Points
	 */
	public static int getNbPoints()
	{
		return nbPoints;
	}

	/**
	 * toString
	 * @return the value of the object as a String
	 */
	public String toString()
	{
		return (new String ("[x] = " + this.getX() + ", [y] = " + this.getY()));
	}

	/**
	 * Move a point to the new coordinates
	 * @param x nouvelle position x
	 * @param y nouvelle position y
	 */
	public void move(int x, int y)
	{
		 this.x += x;
		 this.y += y;
	}
	
	/**
	 * Move a point to the new coordinates
	 * @param p nouvelle position Point2D
	 */
	public void move(Point2D p)
	{
		this.x += p.x;
		this.y += p.y;
	}

	/**
	 * Get the distance between two points
	 * @param p1 Point2D
	 * @return distance entre this et p1
	 */
	public double distance(Point2D p1)
	{
		double dx = this.x - p1.x;
		double dy = this.y - p1.y;
		
		return (Math.sqrt((dx * dx) + (dy * dy)));
	}
	
	/**
	 * Get the distance between two points
	 * @param p1 Point2D
	 * @param p2 Point2D
	 * @return distance entre p1 et p2
	 */
	public static double distance(Point2D p1, Point2D p2)
	{
		return p1.distance(p2);
	}
	
	/**
	 * Is two points equals?
	 * @param p1 Point2D
	 * @param p2 Point2D
	 * @return boolean True if the distance between p1 and p2 is lesser than epsilon 
	 * 					else False
	 */
	public static boolean equal(Point2D p1, Point2D p2)
	{
		return (Point2D.distance(p1, p2) < epsilon);
	}
}