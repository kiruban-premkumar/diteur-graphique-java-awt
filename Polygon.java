import java.awt.Color;
import java.awt.Graphics;
import java.io.EOFException;
import java.io.IOException;
import java.io.Serializable;
import java.util.Vector;

/**
 * Classe Polygon
 * @author Adrien BROYERE
 * @author Kiruban PREMKUMAR
 */

public class Polygon extends FigureGraphique implements Serializable
{
	/**
	 * Liste des points du polygone
	 */
	protected Vector<Point2D> points;
	
	/**
	 * Constructeur
	 * @param name Le nom du polygone
	 * @param outlineColor La couleur de contour
	 * @param fillColor La couleur de remplissage
	 * @param points Les points du polygone
	 */
	public Polygon(String name, Color outlineColor, Color fillColor, Point2D[] points)
	{
		super(name, outlineColor, fillColor);
		this.points = new Vector<Point2D>();
		
		for (int i = 0; i < points.length; i++)
			this.points.add(new Point2D(points[i]));
		
		//	A polygon must have, at least, 3 points
		for (int i = points.length; i < 3; i++)
		{
			//	fill the missing points with the last point of the tab 'points'
			this.points.add(new Point2D(points[points.length - 1]));
		}
	}
	
	/**
	 * Retourne les informations du polygone
	 * @return Les details d'un polygone, sous la forme d'une chaine de carateres
	 */
	public String toString()
	{
		//	Create at string that contains the points of the polygon
		String s = new String("");
		for (int i = 0; i < this.points.size(); i++)
			s += this.points.elementAt(i).toString() + "\n";
		
		return (new String("Polygone : " + this.name + "\nPoints :\n" + s + "Couleur contour : "
				+ this.outlineColor +"\nCouleur remplissage : " + this.fillColor));
	}
	
	/**
	 * Retourne la surface du polygone
	 * @return La surface du polygone, sous la forme d'un double
	 */
	@Override
	public double getSurface()
	{
		// TODO Auto-generated method stub
		
		//	http://alienryderflex.com/polygon_area/
		
		double surface = 0;
		int i, j = this.points.size() - 1;

		for (i = 0; i < this.points.size(); i++)
		{
			surface += (this.points.elementAt(j).x + this.points.elementAt(i).x) * (this.points.elementAt(j).y - this.points.elementAt(i).y);
			j = i;
		}
		
		return surface * 0.5;
	}
	
	/**
	 * Retourne le barycentre du polygone
	 * @return Le barycentre du polygone, sous la forme d'un objet Point2D
	 */
	@Override
	public Point2D getCenter()
	{
		// TODO Auto-generated method stub
		
		//	Moyenne des points de la figure
		int i = 0;
		Point2D centroid = new Point2D();
		
		for (i = 0; i < this.points.size(); i++)
		{
			centroid.x += this.points.elementAt(i).x;
			centroid.y += this.points.elementAt(i).y;
		}
		
		centroid.x = centroid.x / this.points.size();
		centroid.y = centroid.y / this.points.size();
		
		return centroid;
	}
	
	/**
	 * Deplace un polygone
	 * @param x Position sur l'axe X
	 * @param y Position sur l'axe Y
	 */
	@Override
	public void move(int x, int y)
	{
		// TODO Auto-generated method stub
		int i = 0;
		
		for (i = 0; i < this.points.size(); i++)
			this.points.elementAt(i).move(x, y);
		
	}
	
	/**
	 * Verifie si un point est contenu dans le polygone
	 * @param p un point de type Point2D
	 * @return True si le point p est contenu dans le polygone
	 * 		   False sinon
	 */
	@Override
	public boolean contain(Point2D p)
	{
		int i = 0;
		Point2D baselinePoint = new Point2D(points.elementAt(0));
		
		//	Fill the polygon
		for (i = 1; i < (points.size() - 1); i++)
		{
			Triangle triangle = new Triangle(fillColor, fillColor, baselinePoint, points.elementAt(i), points.elementAt(i + 1));
			
			if (triangle.contain(p))
				return true;
		}
		
		return false;
	}
	
	/**
	 * Dessine le polygone
	 * @param g Composant Graphics servant au dessin
	 */
	@Override
	public void drawYourself(Graphics g)
	{
		// TODO Auto-generated method stub
		int i = 0;
		Point2D baselinePoint = new Point2D(points.elementAt(0));
		
		//	Fill color of the polygon
		for (i = 1; i < (points.size() - 1); i++)
		{
			Triangle triangle = new Triangle(fillColor, fillColor, baselinePoint, points.elementAt(i), points.elementAt(i + 1));
			triangle.drawYourself(g);
		}
		
		//	Outline color of the polygon
		g.setColor(outlineColor);
		for (i = 0; i < (points.size() - 1); i++)
		{
			g.drawLine(points.elementAt(i).x, points.elementAt(i).y, points.elementAt(i + 1).x, points.elementAt(i + 1).y);
		}
		
		g.drawLine(points.elementAt(points.size() - 1).x, points.elementAt(points.size() - 1).y, points.elementAt(0).x, points.elementAt(0).y);
		
		//	Draw the name of the polygon
		g.drawString(this.name, this.getCenter().x, this.getCenter().y);
	}
}
