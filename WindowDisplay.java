import java.awt.*;
import java.util.Vector;

/**
 * Classe WindowDisplay
 * @author Adrien BROYERE
 * @author Kiruban PREMKUMAR
 */

class WindowDisplay extends Frame
{
	/**
	 *	Le chemin du fichier � enregistrer ou a ouvrir
	 */
	String filePath;
	
	/**
	 *	La zone de dessin
	 */
	DrawingArea da;
	
	/**
	 * 	La barre d'outils
	 */
	Toolbar outils;
	
	/**
	 *	Composant servant au dessin
	 */
	Graphics gx;
	
	/**
	 * 	Les figures qui ont �t� cr��es
	 */
	Vector<FigureGraphique> figs;
	
	/**
	 *	Les figures s�lectionn�es
	 */
	Vector<FigureGraphique> selectedFigs;
	
	/**
	 * 	L'abscice et l'ordonn�e du bouton gauche de la souris, lorsque celui ci est pr�ss�
	 */
	int leftXPressed, leftYPressed;
	
	/**
	 *	L'abscice et l'ordonn�e du bouton droit de la souris lorsque celui ci est pr�ss�
	 */
	int rightXPressed, rightYPressed;
	
	/**
	 *	La couleur de contour d'une figure
	 */
	Color outlineColor;
	
	/**
	 *	La couleur de remplissage d'une figure
	 */
	Color fillColor;
	
	/**
	 *	Chaine permettant de connaitre le type d'une figure (rectangle, cercle, ...)
	 */
	String figure;
	
	/**
	 *	Chaine permettant de connaitre le nom d'une figure
	 */
	String figureName;
	
	/**
	 *	Vector permettant de stocker les points lors du dessin d'une figure
	 */
	Vector<Point2D> points;
	
	/**
	 * Constructeur de WindowDisplay
	 * @param vec vecteur contenant les FigureGraphique cr��es pr�alablement
	 */
	public WindowDisplay(Vector <FigureGraphique>vec)
	{
		filePath = "";
		setSize(1000,600);
		setTitle("Affichage de Figures Graphiques");
		setForeground(Color.white);
		setBackground(Color.black);
		outlineColor = Color.black;
		fillColor = Color.white;
		figure = "Cercle";
		figureName = "";
		figs = vec;
		selectedFigs = new Vector<FigureGraphique>();
		add(da = new DrawingArea(this),"Center");
		
		//$$1 - fait
		addWindowListener(new WindowController(this));
		
		//	Ajout de la barre d'outils
		add(outils = new Toolbar(this), "South");
		
		// barre de menu
		setMenuBar(new Menubar(this));
		
		setVisible(true);
		Graphics gx = da.getGraphics();
		
		//	Dessin des figures cr��es pr�alablement
		dessineFigs(gx);
		
		points = new Vector<Point2D>();
	}
	
	/**
	 * M�thode permettant de savoir si un fichier a �t� enregistr�
	 * @return un bool�en confirmant si un fichier a �t� enregistr�
	 */
	public boolean isFileNotSaved()
	{
		return filePath.isEmpty();
	}
	
	/**
	 * M�thode permettant de dessiner les figures contenu dans le vector figs
	 * @param g Composant servant au dessin
	 */
	public void dessineFigs(Graphics g)
	{
		for (int i= 0; i< figs.size(); i++)
			figs.get(i).drawYourself(g);
	}
	
	/**
	 * M�thode permettant de changer la couleur de contour d'une figure
	 * @param color La nouvelle couleur
	 */
	public void changeCouleurContour(Color color)	
	{
		outlineColor = color;
	}
	
	/**
	 * M�thode permettant de changer la couleur de remplissage d'une figure
	 * @param color La nouvelle couleur
	 */
	public void changeCouleurRemplissage(Color color)
	{
		fillColor = color;
	}
	
	/**
	 * M�thode permettant de connaitre le type de figure s�lectionn�
	 * @param fig Chaine indiquant le type de figure
	 */
	public void changeFigure(String fig)
	{
		figure = fig;
	}
	
	/**
	 * M�thode permettant de changer le nom d'une figure
	 * @param figureName Le nouveau nom de la figure
	 */
	public void changeFigureName(String figureName)
	{
		this.figureName = figureName;
	}
	
	/**
	 * M�thode permettant de garder en m�moire les coordonn�es de la souris lors de l'appui sur le bouton gauche
	 * @param x L'abscisse du pointeur de la souris
	 * @param y L'ordonn�e du pointeur de la souris
	 */
	public void boutonSourisEnfonce(int x, int y)
	{
		this.leftXPressed = x;
		this.leftYPressed = y;
	}
	
	/**
	 * M�thode permettant de commencer le dessin d'une figure lors du relachement du clic gauche de la souris
	 * @param x L'abscisse du pointeur de la souris
	 * @param y L'ordonn�e du pointeur de la souris
	 */
	public void boutonSourisRelache(int x, int y)
	{
		Graphics g = this.da.getGraphics();
		
		if (figure.equals("Cercle"))
			drawCircle(x, y);
		else if (figure.equals("Rectangle"))
			drawRectangle(x, y);
		else if (figure.equals("Triangle"))
			drawTriangle(x, y, g);
		else
			drawPolygon(x, y, g);
		
		this.dessineFigs(g);
	}
	
	/**
	 * M�thode permettant de s�lectionn� une figure lors de l'appui du bouton droit de la souris
	 * @param x	L'abscisse du pointeur de la souris
	 * @param y L'ordonn�e du pointeur de la souris
	 */
	public void boutonDroitSourisEnfonce(int x, int y)
	{
		rightXPressed = x;
		rightYPressed = y;
		
		//	M�thode appel� pour d�terminer la ou les figures s�lectionn�es
		getSelectedFigs(x, y);
	}
	
	/**
	 * M�thode appel� lors du relachement du bouton droit de la souris, cette m�thode permet le d�placement des figures
	 * @param x	L'abscisse du pointeur de la souris
	 * @param y L'ordonn�e du pointeur de la souris
	 */
	public void boutonDroitSourisRelache(int x, int y)
	{
		//	M�thode permettant de faire bouger les figures
		moveFigs(x, y);
	}
	
	/**
	 * Methode permettant de dessin� un cercle
	 * @param x L'abscisse du pointeur de la souris
	 * @param y L'ordonn�e du pointeur de la souris
	 */
	private void drawCircle(int x, int y)
	{
		//		DRAW A CIRCLE
		int radius = 0;
		if (x < leftXPressed || y < leftYPressed)
			radius = Math.abs(Math.min(x - leftXPressed, y - leftYPressed));
		else
			radius = Math.abs(Math.max(x - leftXPressed, y - leftYPressed));
			
		Circle cercle = new Circle(figureName, outlineColor, fillColor, leftXPressed, leftYPressed, radius);
		
		this.figs.add(cercle);
		//	END DRAW CIRCLE
	}
	
	/**
	 * Methode permettant de dessin� un rectangle
	 * @param x L'abscisse du pointeur de la souris
	 * @param y L'ordonn�e du pointeur de la souris
	 */
	private void drawRectangle(int x, int y)
	{
		//		DRAW A RECTANGLE
		Point2D origin = new Point2D();
		int width = 0, height = 0;
		
		//	Rectangle drawn from top left to bottom right
		if (x >= leftXPressed && y >= leftYPressed)
		{
			origin.x = leftXPressed;
			origin.y = leftYPressed;
			width = x - leftXPressed;
			height = y - leftYPressed;
		}
		//	Rectangle drawn from bottom left to top right
		else if (x >= leftXPressed && y <= leftYPressed)
		{
			origin.x = leftXPressed;
			origin.y = y;
			width = x - leftXPressed;
			height = leftYPressed - y;
		}
		//	Rectangle drawn from top right to bottom left
		else if (x <= leftXPressed && y >= leftYPressed)
		{
			origin.x = x;
			origin.y = leftYPressed;
			width = leftXPressed - x;
			height = y - leftYPressed;
		}
		//	Rectangle drawn from bottom right to top left
		else
		{
			origin.x = x;
			origin.y = y;
			width = leftXPressed - x;
			height = leftYPressed - y;
		}
		
		//	Create the rectangle
		Rectangle rectangle = new Rectangle(figureName, outlineColor, fillColor, origin.x, origin.y, width, height);
		
		this.figs.add(rectangle);
		//	END DRAW A RECTANGLE	
	}
	
	/**
	 * Methode permettant de dessin� un triangle
	 * @param x L'abscisse du pointeur de la souris
	 * @param y L'ordonn�e du pointeur de la souris
	 * @param g Composant servant au dessin
	 */
	private void drawTriangle(int x, int y, Graphics g)
	{
		//		DRAW A TRIANGLE
		
		//	The first line
		if (points.size() == 0)
		{
			points.add(new Point2D(leftXPressed, leftYPressed));
			points.add(new Point2D(x, y));
			g.setColor(outlineColor);
			g.drawLine(leftXPressed, leftYPressed, x, y);
		}
		else
		{
			Triangle triangle = new Triangle(figureName, outlineColor, fillColor, points.elementAt(0), points.elementAt(1), new Point2D(x, y));
			figs.add(triangle);
					
			points.clear();	//delete all points of the vector
		}
		//	END DRAW A TRIANGLE	
	}
	
	/**
	 * Methode permettant de dessin� un polygone
	 * @param x L'abscisse du pointeur de la souris
	 * @param y L'ordonn�e du pointeur de la souris
	 * @param g Composant servant au dessin
	 */
	private void drawPolygon(int x, int y, Graphics g)
	{
		// DRAW A POLYGON
		
		// the first line
		if (points.size() == 0)
		{
			points.add(new Point2D(leftXPressed, leftYPressed));
			points.add(new Point2D(x, y));
			g.setColor(outlineColor);
			g.drawLine(leftXPressed, leftYPressed, x, y);
		}
		else
		{
			// If the end of the last line meet the first point, the polygon is ready 
			if (Point2D.equal(new Point2D(x, y), points.elementAt(0)))
			{
				//	A polygon must have, at least, 3 points
				if (points.size() > 2)
				{
					points.add(new Point2D(x, y));
					Polygon polygon = new Polygon(figureName, outlineColor, fillColor, points.toArray(new Point2D[points.size()]));
					figs.add(polygon);
							
					points.clear();
				}
						
				//	ELSE DO NOTHING - Wait for the user to create a new point
			}
			else
			{
				g.setColor(outlineColor);
				g.drawLine(points.elementAt(points.size() - 1).x, points.elementAt(points.size() - 1).y, x, y);
				points.add(new Point2D(x, y));
			}
		}
		
		//END DRAW A POLYGON
	}
	
	/**
	 * Retour les figures selectionn�es
	 * @param x L'abscisse du pointeur de la souris
	 * @param y L'ordonn�e du pointeur de la souris
	 */
	private void getSelectedFigs(int x, int y)
	{
		int i = 0;
		Point2D point = new Point2D(x, y);
		
		//	Run through the created figs
		for (i = 0; i < figs.size(); i++)
		{
			//	If the click was on a figs
			if (figs.get(i).contain(point))
			{
				//	If the figs was already selected, we deselect the figs
				if (selectedFigs.contains(figs.get(i)))
					selectedFigs.remove(figs.get(i));
				//	Else we add the figs to the selected figs
				else
					selectedFigs.add(figs.get(i));
			}
		}
		
		/*for (i = 0; i < selectedFigs.size(); i++)
			System.out.println(selectedFigs.get(i).toString());*/
		
		System.out.println("Nombre elements selectionnes = " + selectedFigs.size());
	}
	
	/**
	 * Permet d'effacer les figures
	 */
	public void deleteFigs()
	{
		Graphics g = this.da.getGraphics();
		int i;
		
		//	Run through the selected figs
		for (i = 0; i < selectedFigs.size(); i++)
		{
			//	Remove the selected figs from the main figs list
			figs.remove(selectedFigs.get(i));
		}
		
		//	Once all selectedFigs where removed from the main list, the selectedFigs vector can be cleared
		selectedFigs.clear();
		
		update(g);
		da.paint(g);
	}
	
	/**
	 * Setter pour la d�finition d'un chemin d'enregistrement
	 * @param f Le chemin du fichier
	 */
	public void setFilePath(String f) {
		this.filePath = f;
	}
	
	/**
	 * Getter pour les figures
	 * @return les figures
	 */
	public Vector<FigureGraphique> getFigs() {
		return figs;
	}
	
	/**
	 * Permet de definir des figures � partir d'un fichier par exemple
	 * @param figs Liste des figures
	 */
	public void setFigs(Vector<FigureGraphique> figs) {
		this.figs = figs;
	}
	
	/**
	 * Permet de d�placer les figures selectionn�es
	 * @param x L'abscisse du pointeur de la souris
	 * @param y L'ordonn�e du pointeur de la souris
	 */
	private void moveFigs(int x, int y)
	{
		Graphics g = this.da.getGraphics();
		int i;
		
		for (i = 0; i < figs.size(); i++)
		{
			if (selectedFigs.contains(figs.get(i)))
				figs.get(i).move(x - rightXPressed, y - rightYPressed);
		}
		
		update(g);
		da.paint(g);
	}
	
}

