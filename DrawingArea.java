import java.awt.*;

/**
 * Classe DrawingArea
 * @author Adrien BROYERE
 * @author Kiruban PREMKUMAR
 */

class DrawingArea extends Canvas
{
	/**
	 * Fenetre des elements graphiques
	 */
	private WindowDisplay frame;
	
	/**
	 * Constructeur
	 * @param frame Fenetre des elements graphiques
	 */
	public DrawingArea(WindowDisplay frame)
	{
		this.frame = frame;
		setBackground(Color.white);
		
		//$$3 - fait
		addMouseListener(new MouseManager(frame));
		addKeyListener(new ControlSupprFigure(frame));
	}
	
	/**
	 * Dessiner les figures
	 * @param g Composant Graphics servant au dessin
	 */
	public void paint(Graphics g)
	{
		//$$2 - fait
		this.frame.dessineFigs(g);
	}
}