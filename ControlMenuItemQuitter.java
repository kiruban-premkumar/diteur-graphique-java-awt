import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 * Classe ControlMenuItemQuitter
 * @author Adrien BROYERE
 * @author Kiruban PREMKUMAR
 */

public class ControlMenuItemQuitter implements ActionListener 
{
	/**
	 * Fenetre des elements graphiques
	 */
	private WindowDisplay frame;
	
	/**
	 * Constructeur
	 * @param f Fenetre des elements graphiques
	 */
	public ControlMenuItemQuitter(WindowDisplay f) {
		// TODO Auto-generated constructor stub
		frame = f;
	}
	
	/**
	 * Listener pour quitter l'application
	 * @param e ActionEvent
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		int diagReturn = JOptionPane.showConfirmDialog(frame, "Voulez-vous quitter?", "Quitter", 2);
		if (diagReturn == JOptionPane.OK_OPTION)
			System.exit(0);
	}

}
