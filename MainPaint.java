import java.awt.Color;
import java.util.Vector;

/**
 * Classe MainPaint
 * @author Adrien BROYERE
 * @author Kiruban PREMKUMAR
 */

public class MainPaint
{
	/**
	 * Fonction main de l'application
	 * @param args arguments pour l'�x�cution en ligne de commande
	 */
	public static void main(String[] args)
	{
		Vector<FigureGraphique> toto = new Vector<FigureGraphique>();
		Triangle triangle = new Triangle("triangle", Color.black, Color.blue, 100, 300, 150, 150, 200, 300);
		Rectangle rectangle = new Rectangle("rectangle", Color.black, Color.red, 250, 150, 50, 150);
		
		Point2D[] points = {new Point2D(350, 200), new Point2D(400, 150), new Point2D(450, 150), new Point2D(500, 200), new Point2D(500, 250), new Point2D(450, 300), new Point2D(400, 300), new Point2D(350, 250)};
		Polygon polygon = new Polygon("polygone", Color.black, Color.yellow, points);
		
		Circle circle = new Circle("cercle", Color.black, Color.green, 600, 225, 50);
		
		toto.add(triangle);
		toto.add(rectangle);
		toto.add(polygon);
		toto.add(circle);
		
		new WindowDisplay(toto);
	}

}
